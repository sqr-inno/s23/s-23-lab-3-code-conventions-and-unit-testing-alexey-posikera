package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ThreadControllerTest {
    private Thread thread;
    private Thread new_thread;
    private final String new_slug = "new_5lug";
    private final int new_threadId = 0;
    private Vote vote;
    private final String slug = "5lug";
    private final int threadId = 0xdead;
    private final String nickname = "some";
    List<Post> posts;
    private ThreadController threadController;

    private MockedStatic<ThreadDAO> threadMock;
    private MockedStatic<UserDAO> userMock;

    @BeforeEach
    void setUp() {
        threadController = new ThreadController();
        String email = "som@email.com";
        String fullname = "name";
        String about = "nothing";
        User user = new User(nickname, email, fullname, about);
        Date date = new Date();
        String forum = "some forum";
        String message = "some message";
        String title = "some title";
        int votes = 0;
        thread = new Thread(nickname, new Timestamp(date.getTime()), forum, message, slug, title, votes);
        thread.setId(threadId);
        new_thread = new Thread(nickname, new Timestamp(date.getTime()), forum, message, new_slug, title, votes);
        new_thread.setId(new_threadId);
        Post post = new Post(nickname, new Timestamp(date.getTime()), forum, message, 0, 0, false);
        posts = new ArrayList<>();
        posts.add(post);
        vote = new Vote(nickname, 1);

        threadMock = Mockito.mockStatic(ThreadDAO.class);
        userMock = Mockito.mockStatic(UserDAO.class);
        threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
        threadMock.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
        threadMock.when(() -> ThreadDAO.getPosts(threadId, 1, 0, "tree", false)).thenReturn(posts);
        threadMock.when(() -> ThreadDAO.getThreadBySlug(new_slug)).thenReturn(new_thread);
        threadMock.when(() -> ThreadDAO.getThreadById(new_threadId)).thenReturn(new_thread);
        threadMock.when(() -> ThreadDAO.createVote(thread, vote)).then(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                thread.setVotes(vote.getVoice());
                return null;
            }
        });
        userMock.when(() -> UserDAO.Info(nickname)).thenReturn(user);
    }

    @AfterEach
    void close() {
        threadMock.close();
        userMock.close();
    }

    @Test
    void checkIdOrSlug() {
        assertEquals(thread, threadController.CheckIdOrSlug(slug));
        assertEquals(thread, threadController.CheckIdOrSlug(Integer.toString(threadId)));
    }

    @Test
    void createPost() {
        ResponseEntity r = threadController.createPost(slug, posts);
        assertEquals(HttpStatus.CREATED, r.getStatusCode());
        assertEquals(posts, r.getBody());
    }

    @Test
    void posts() {
        ResponseEntity r = threadController.Posts(slug, 1, 0, "tree", false);
        assertEquals(HttpStatus.OK, r.getStatusCode());
        assertEquals(posts, r.getBody());
    }

    @Test
    void change() {
        ResponseEntity r = threadController.change(new_slug, new_thread);
        assertEquals(HttpStatus.OK, r.getStatusCode());
        assertEquals(r.getBody(), new_thread);
    }

    @Test
    void info() {
        ResponseEntity r = threadController.info(new_slug);
        assertEquals(HttpStatus.OK, r.getStatusCode());
        assertEquals(r.getBody(), new_thread);
    }

    @Test
    void createVote() {
        assertEquals(0, (int)thread.getVotes());
        ResponseEntity r = threadController.createVote(slug, vote);
        assertEquals(HttpStatus.OK, r.getStatusCode());
        assertEquals(r.getBody(), thread);
        assertEquals(1, (int)thread.getVotes());
    }
}